## No.1
Berikut adalah algoritma dari main app untuk melihat list movie dan series yang tersedia, menonton movie dan series yang tersedia, dan bookmark movie dan series:


**Algoritma Deskriptif**

![Algoritma_Deskriptif](Screenshot Algoritma/Algoritma_Deskriptif.jpg)

**Algoritma Pseudocode**

![Algoritma_Pseudocode](Screenshot Algoritma/Algoritma_Pseudocode.jpg)

**Algoritma Bahasa Java**
![Algoritma_Source_Code_Java](Screenshot Algoritma/Algoritma_Source_Code_Java.png)


## No.2

Algoritma yang sudah saya berikan di no. 1 kemudian diimplemetasikan ke dalam source code. Dari program yang saya buat algoritma itu berada dalam kelas viu. Berikut adalah source code-nya:

[Viu.java](MainApp/Viu.java)

Untuk source code kelas lainnya

kelas Video:
[Video.java](MainApp/Video.java)

kelas Movie:
[Movie.java](MainApp/Movie.java)

kelas Series:
[Series.java](MainApp/Series.java)

kelas EpisodeSeries:
[EpisodeSeries.java](MainApp/EpisodeSeries.java)

kelas User:
[User.java](MainApp/User.java)

kelas Main:
[Main.java](MainApp/Main.java)

## No.3
Konsep Dasar OOP, yaitu ada objek, class, attribute, dan methode.

- Objek : Objek merupakan segala sesuatu yang ada di dunia nyata. Setiap objek memiliki atribut dan bisa melakukan tindakan (method). Objek adalah bentuk atau contoh nyata dari class.
- Class : Class merupakan template/kerangka untuk membuat objek. Class menggambarkan apa yang objek dapat lakukan dan bagaimana objek tersebut berinteraksi dengan objek lain. Class berupa kumpulan atas definisi data dan fungsi dalam suatu unit untuk suatu tujuan tertentu.
Contohnya _ada class of cat atau kelas kucing yang mendefinisikan suatu unit yang terdiri atas definisi-definisi data dan fungsi-fungsi yang menunjuk pada beberapa macam perilaku dari kucing._
- Attribute : Attribute atau property adalah data yang membedakan antara objek satu dengan objek yang lainnya.
- Method : Method atau disebut juga tingkah laku adalah hal-hal yang bisa dilakukan objek dari suatu class.

Contoh : 

Objek  >> Manusia; Atribut >> tinggi, umur, dan berat; Method >> Berjalan, Berlari, Makan, Minum, dan sebagainya.

Di dalam OOP juga ada 4 pilar, yaitu: Abstraction, Encapsulation, Inheritance, dan Polymorphism.

## No.4
**Encapsulation**

Encapsulation secara sederhana adalah cara untuk menggabungkan data dan fungsi terkait ke dalam satu objek. Tujuannya adalah melindungi data agar tidak dapat diakses atau dimodifikasi secara langsung dari luar objek. Dengan menggunakan metode, kita dapat mengontrol akses ke data dan memastikan bahwa data hanya dapat diubah melalui prosedur yang ditentukan. Dengan encapsulation, kita dapat menyembunyikan detail implementasi objek dan hanya mengekspos metode publik yang relevan. Hal ini membantu dalam memisahkan bagaimana objek digunakan dan bagaimana objek tersebut diimplementasikan secara internal, sehingga memudahkan pemeliharaan dan pengembangan program. 
Encapsulation berhubungan dengan Access Modifiers. Access Modifiers digunakan untuk mengatur tingkat aksesibilitas atribut dan metode dalam suatu kelas. Terdapat beberapa jenis Access Modifiers seperti public, private, protected, dan default (tanpa deklarasi akses modifier). Public memungkinkan akses dari mana saja, private hanya dapat diakses di dalam kelas itu sendiri, protected dapat diakses oleh kelas yang sama dan kelas turunannya, dan default hanya dapat diakses oleh kelas dalam package yang sama. Lalu di dalam encapsulation juga ada Getter dan Setter Method. Getter dan setter methods digunakan untuk mengakses dan mengubah nilai atribut yang berada dalam suatu kelas. Getter method digunakan untuk mendapatkan nilai atribut, sedangkan setter method digunakan untuk mengubah nilai atribut. Dengan menggunakan getter dan setter methods, kita dapat mengatur pengaksesan dan modifikasi nilai atribut dengan kontrol yang lebih baik.

Contoh penggunaan encapsulation di salah satu source code pada program yang telah dibuat : 

saya mengambil contoh dari class User

![Screenshot_2023-05-12_222631](/uploads/af4e5717f2c314f254de7a664a3bcd2d/Screenshot_2023-05-12_222631.png)

Dalam contoh ini, Variabel `email`, `password`, `bookmarkList`, dan `bookmarkSeriesList` dideklarasikan sebagai private alasannya agar variabel-variabel ini tidak dapat diakses langsung dari luar kelas User. Sehingga metode-metode `getEmail()`, `setEmail()`, `getPassword()`, `setPassword()`, `getBookmark()`, `addBookmark()`, `getBookmarkSeriesList()`, dan `addBookmarkSeries()` yang memberikan akses terkendali ke variabel-variabel private tersebut.

Encapsulation memungkinkan kontrol lebih baik terhadap data dan operasi yang dilakukan pada data. Misalnya, metode `setEmail()` dan `setPassword() `memungkinkan validasi data sebelum nilai-nilai baru diterapkan ke variabel `email` dan `password`. Dengan menggunakan encapsulation, kita dapat memastikan bahwa data sensitif seperti _email_ dan _password_ hanya dapat diakses melalui metode-metode yang sesuai, dan penggunaan metode-metode ini dapat dikontrol dengan validasi data yang tepat.

## No.5
**Abstraction**

Abstraction dalam pemrograman adalah cara untuk menyederhanakan  informasi yang penting dari suatu objek atau sistem, dengan sementara menyembunyikan detail yang tidak relevan atau kompleks. Dalam OOP, Abstraction memungkinkan kita untuk fokus pada informasi penting seperti metode dan atribut yang relevan pada suatu objek, tanpa harus khawatir tentang bagaimana implementasinya di dalam kelas tersebut. Dengan demikian, Abstraction membantu kita untuk membagi kompleksitas menjadi bagian-bagian yang lebih kecil dan memahami objek atau sistem hanya dengan informasi yang diperlukan. Dalam praktiknya, Abstraction diterapkan dengan membuat kelas abstrak yang memiliki metode abstrak (yang tidak memiliki implementasi) untuk mewakili konsep umum, dan kemudian kelas turunan mewarisi kelas abstrak tersebut dan memberikan implementasi sesuai dengan detail khusus yang diperlukan.
Dengan menggunakan Abstraction, pemrograman dapat fokus pada pemecahan masalah dan desain yang lebih tinggi, sambil menyembunyikan detail yang kompleks atau tidak relevan, sehingga membuat pemrograman menjadi lebih terstruktur, modular, dan mudah dipahami. 

Contoh penggunaan abstraction:

![Screenshot_2023-05-12_223758](/uploads/01dd01a31377aa89bdc77bca00d6ed9f/Screenshot_2023-05-12_223758.png)

Kelas _Video_ memiliki sebuah metode abstrak yaitu "menonton()" untuk menonton sebuah video. Karena kelas Video adalah kelas abstrak, maka tidak dapat diinisialisasi objeknya secara langsung. Maka, kelas turunan harus mengimplementasikan metode-metode abstrak ini. 

## No.6
**Inheritance**

Inheritance atau pewarisan dalam OOP adalah konsep yang memungkinkan kita untuk membuat hubungan hierarki antara kelas-kelas. Di dalam inheritance itu ada superclass dan subclass, yang di mana superclass adalah kelas dasar dari subclass atau dimisalkan superclass adalah kelas induk dari turunan-turunannya (subclass). 

**Polymorphism**

Polymorphism adalah konsep dalam OOP yang memungkinkan objek dengan jenis yang berbeda untuk diakses dan diproses menggunakan cara yang sama. Misalnya, kita punya kelas "Hewan" dan ada kelas turunannya seperti "Kucing" dan "Anjing". Meskipun mereka berbeda jenis, kita tetap bisa memanggil metode yang sama pada keduanya. 
Polymorphism sering terjadi dalam konteks pewarisan, sehingga polymorphism ini erat kaitannya dengan inheritance atau pewarisan. Jadi, dalam contoh tadi, "Kucing" dan "Anjing" mewarisi sifat dan metode dari kelas "Hewan" yang lebih umum. Ketika kita memanggil metode pada objek "Kucing" atau "Anjing" melalui referensi yang bertipe "Hewan", metode yang sesuai akan dieksekusi.
Dengan menggunakan polymorphism, kita bisa mengelompokkan objek-objek dengan tipe yang berbeda menjadi satu tipe yang lebih umum. Misalnya, kita bisa menggunakan referensi "Hewan" untuk mengakses objek "Kucing" atau "Anjing". Hal ini memudahkan penggunaan objek dengan fleksibilitas yang tinggi, mengurangi penulisan kode yang berulang, dan memungkinkan penggunaan pola desain yang efisien.

Berikut contoh penggunaannya:

- Kelas abstrak menjadi superclass

![Screenshot_2023-05-12_223758](/uploads/01dd01a31377aa89bdc77bca00d6ed9f/Screenshot_2023-05-12_223758.png)

- Kelas Movie dan EpisodeSeries menjadi turunannya (subclass)

![Screenshot_2023-05-12_224536](/uploads/4e77a688049be685772114d1980c857e/Screenshot_2023-05-12_224536.png)
![Screenshot_2023-05-12_224456](/uploads/a80bc68fe5292c35a9a04884ef18a773/Screenshot_2023-05-12_224456.png)

- contoh polymorphism

Untuk pengunaan fungsi play() dari objek kelas _EpisodeSeries_ ada ada pada kelas _Series_ karena diolah dulu di kelas _Series_

![Screenshot_2023-05-12_225111](/uploads/e3fb8de0d89b70d46ab79787a548b15c/Screenshot_2023-05-12_225111.png)

jadi _episodeSeriesList.get(episode)_ merupakan objek dari kelas EpisodeSeries yang melakukan fungsi play()


Selanjutnya penggunaan fungsi play() dari objek kelas _Movie_ ada pada kelas _Viu_ 
![Screenshot_2023-05-12_230011](/uploads/d0ace8a12f0a254ce2a9d62ac7b0412c/Screenshot_2023-05-12_230011.png)

jadi _movie_  merupakan objek dari kelas _Movie_ yang melakukan fungsi play()

## No.7
Menerjemahkan Use Case yang sudah dibuat ke dalam OOP:

- Untuk semua Use Case karena dilakukan di aplikasi viu maka yang pasti akan ada Class **_Viu_** sebagai main class.

- Untuk Use Case yang pertama itu dapat "menonton video berupa movie atau series", berarti objek yang utama yaitu Video, maka buatlah class **_Video_**. Lalu ada objek movie dan series, maka buatlah class **_Movie_**, **_Series_**, dan **_EpisodeSeries_**. Class **_Video_** akan menjadi abstract class, lalu Class **_Movie_** menjadi kelas turunannya, dan untuk series Class **_EpisodeSeries_** yang menjadi turunannya.

- Untuk Use Case _"mendaftar akun baru dengan memasukkan email dan password"_, _"login ke dalam akun yang telah didaftarkan sebelumnya"_, _"logout dari akun VIU"_ ada Class **_User_** dengan attribut yang dibutuhkan itu `email` dan `password`. Lalu untuk method mendaftar dan log in terjadi di dalam Class **_Viu_**

- Untuk Use Case _"menambahkan video ke dalam bookmark list"_ dan _"menghapus video dari dalam bookmark list"_ ditambahkan attribut arraylist untuk bookmark list di dalam Class **_User_**, lalu akan ditambahakan method _akses bookmark_ dan method _add bookmark_ di dalam Class **_User_**

## No.8
**Use Case Table**

| No      | Actor      | Dapat  | Prioritas |
| ------  | ------     | ------ | ------ |
| 1 |User        |menonton video berupa movie atau series| 90 |
| 2 |User        |melihat daftar movie dan series yang tersedia| 80 |
| 3 |User        |memilih untuk menonton movie atau series tertentu| 85 |
| 4 |User        |mencari movie atau series tertentu| 75 |
| 5 |User        |mendaftar akun baru dengan memasukkan email dan password| 70 |
| 6 |User        |login ke dalam akun yang telah didaftarkan sebelumnya|75 |
| 7 |User        |mengubah kualitas video| 60 |
| 8 |User        |melihat dan memilih subtitle| 65 |
| 9 |User        |mendapatkan rekomendasi video (movie atau series)| 80 | 
| 10 |User        |melihat list movie atau series berdasarkan kategori| 70 |
| 11 |User        |melihat informasi dari movie atau series yang akan ditonton| 75 |
| 12 |User        |membagikan link dari sebuah movie atau series| 60 |
| 13 |User        |menonton trailer dari movie atau series tertentu sebelum memutarnya| 55 |
| 14 |User        |menambahkan video ke dalam bookmark list| 50 |
| 15 |User        |melihat bookmark list| 40 |
| 16 |User        |menghapus video dari dalam bookmark list| 45 |
| 17 |User        |subscribe paket premium| 80 |
| 18 |User        |User dapat memilih paket langganan (subscribe) premium| 75 |
| 19 |User        |tukar kode voucher| 65 |
| 20 |User        |User dapat memilih metode pembayaran untuk subscribe premium| 70 |
| 21 |User        |mengubah bahasa aplikasi| 55 |
| 22 |User        |menerima notifikasi dari VIU| 60 |
| 23 |User        |menautkan akun VIU di televisi| 70 |
| 24 |User        |melihat status akun pengguna| 50 |
| 25 |User        |melihat FAQ (pertanyaan yang sering ditanyakan)| 45 |
| 26 |User        |melihat jawaban-jawaban mengenai pertanyaan yang sering ditanyakan (FAQ)| 40 |
| 27 |User        |menghubungi VIU| 55 |
| 28 |User        |melakukan pengaduan| 60 |
| 29 |User        |User dapat mengunduh video| 75 |
| 30 |User        |User dapat melihat list video yang diunduh| 65 |
| 31 |User        |User dapat menonton video yang sudah diunduh secara offline| 70 |
| 32 |User        |logout dari akun VIU| 70 |
| 33 |Manajeman(Admin)|mengunggah konten baru beserta informasi konten| 90 |
| 34 |Manajeman(Admin)|mengedit informasi konten yang sudah ada| 85 |
| 35 |Manajeman(Admin)|menghapus konten yang tidak relevan| 80 |
| 36 |Manajeman(Admin)|mengatur pengaturan regional untuk membatasi akses konten tertentu hanya untuk wilayah tertentu| 85 |
| 37 |Manajeman(Admin)|mengelola informasi user, seperti data pribadi, preferensi, atau riwayat tontonan| 80 |
| 38 |Manajeman(Admin)|menghapus atau menangani akun user yang melanggar kebijakan|75|
| 39 |Manajeman(Admin)|menerima dan menjawab keluhan atau pertanyaan user| 70 |
| 40 |Manajeman(Admin)|mengakses laporan dan analitik untuk memantau kinerja aplikasi Viu, seperti statistik penonton, durasi tontonan, atau tingkat interaksi pengguna| 75 |
| 41 |Manajeman(Admin)|mengirim notifikasi kepada user tentang konten baru, pembaruan, atau penawaran khusus| 70 |
| 42 |Manajeman(Admin)|mengatur dan mengonfigurasi pengaturan aplikasi Viu, seperti bahasa, preferensi tampilan, opsi suara, atau pengaturan keamanan| 65 |
| 43 |Manajeman(Admin)|mengelola kategori dan genre konten yang tersedia di aplikasi Viu| 75 |
| 44 |Manajeman(Admin)|menambahkan kategori baru, menghapus kategori yang tidak relevan, atau memperbarui informasi terkait kategori dan genre| 75 |
| 45 |Manajeman(Admin)|memperbarui aplikasi Viu dengan fitur baru, perbaikan bug, atau peningkatan performa| 80 |


**Class Diagram**

![sssss_page-0001](/uploads/23b66426e6bf43584ad54db605392989/sssss_page-0001.jpg)
![implementasiAppViu](/uploads/2586a3521585296bb52d36b20efaa9bb/implementasiAppViu.jpg)

## No.9

Berikut Link Video Youtube-nya:

[Implementasi Dalam Bentuk Program Sederhana Berbasis OOP untuk Aplikasi VIU (Mata Kuliah: PBO)](https://youtu.be/z1PENDbP3lU)

## No.10

Use Case yang dibuat dalam program:

1. User dapat mendaftar akun baru dengan memasukkan email dan password
2. User dapat login ke dalam akun yang telah didaftarkan sebelumnya
3. User dapat melihat daftar movie dan series yang tersedia
4. User dapat menonton video
5. User dapat memilih untuk menonton movie atau series tertentu
6. User dapat menonton trailer dari movie atau series tertentu sebelum memutarnya
7. User dapat menambahkan video ke dalam bookmark list
8. User dapat melihat bookmark list
9. User daapat menghapus video dari dalam bookmark list
10. User dapat logout dari akun VIU

![video_program_setelah_di-run](/uploads/b6b7b19cc5915bd72df7ce53ff9541c5/video_program_setelah_di-run.mp4)
