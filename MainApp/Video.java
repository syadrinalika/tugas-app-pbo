public abstract class Video{
    protected String judul;

    public Video(String judul){
        this.judul = judul;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public abstract void play();
}