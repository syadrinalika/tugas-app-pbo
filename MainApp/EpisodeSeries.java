public class EpisodeSeries extends Video {
    protected int episode;

    public EpisodeSeries(String judul, int episode) {
        super(judul);
        this.episode = episode;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

    public int getEpisode() {
        return episode;
    }

    @Override
    public void play() {
        System.out.println(" Episode " + episode + " : " + judul);
    }

    @Override
    public String toString() {
        return "Episode " + episode + " : " + judul;
    }
}
