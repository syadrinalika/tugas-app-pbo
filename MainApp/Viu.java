import java.util.ArrayList;
import java.util.Scanner;

public class Viu {
    private ArrayList<Movie> movieList;
    private ArrayList<Series> seriesList;
    private ArrayList<User> usersList;
    private Scanner input;

    public Viu(){
        movieList = new ArrayList<>();
        seriesList = new ArrayList<>();
        usersList = new ArrayList<>();

        movieList.add(new Movie("Spiderman Far From Home", true));
        movieList.add(new Movie("Shazam Fury of the Gods", true));
        movieList.add(new Movie("Jumanji", false));
        movieList.add(new Movie("Pengabdi Setan", true));
        movieList.add(new Movie("Fury", true));

        seriesList.add(new Series("Loki Season 1", new String[]{"Glorious Purpose", 
        "The Variant", "Lamentis", "The Nexus Event", "Journey Into Mystery", "For All Time. Always."}, true));
        seriesList.add(new Series("Black Mirror Season 1", new String[]{"The National Anthem",
        "Fifteen Million Merits", "The Entire History of You"}, false));

        input = new Scanner(System.in);
    }

    private boolean login(String email, String password){
        boolean isExist = false;
        for(User user : usersList){
            if(user.getEmail().equals(email) && user.getPassword().equals(password)){
                isExist = true;
                break;
            }
        }

        if(isExist){
            return true;
        }
        return false;
    }

    private User getUser(String email, String password){
        for(User user : usersList){
            if(user.getEmail().equals(email) && user.getPassword().equals(password)){
                return user;
            }
        }

        return null;
    }

    private boolean signup(String email, String password){
        boolean isExist = false;
        for(User user : usersList){
            if(user.getEmail().equals(email)){
                isExist = true;
                break;
            }
        }

        if(!isExist){
            usersList.add(new User(email, password));
            return true;
        }
        return false;
    }

    private void main(User user){
        int choice, tonton, episode;
        char confirm;

        do{
            System.out.println("Selamat Datang di Viu!");
            System.out.println("1. List Movie");
            System.out.println("2. List Series");
            System.out.println("3. Tonton Film");
            System.out.println("4. Tonton Series");
            System.out.println("5. Bookmark");
            System.out.println("6. Keluar");
            System.out.print(">> ");
            choice = Integer.parseInt(input.nextLine());

            System.out.println();
            switch(choice){
                case 1:{
                    if(movieList.size() > 0){
                        System.out.println("List Movie :");
                        for(int i=0; i<movieList.size(); i++){
                            System.out.println((i+1) + ". " + movieList.get(i));
                        }
                    }else{
                        System.out.println("List movie belum tersedia :(");
                    }

                    break;
                }

                case 2:{
                    if(seriesList.size() > 0){
                        System.out.println("List Series :");
                        for(int i=0; i<seriesList.size(); i++){
                            System.out.println((i+1) + ". " + seriesList.get(i));
                        }
                    }else{
                        System.out.println("List series belum tersedia :(");
                    }

                    break;
                }

                case 3:{
                    if(movieList.size() > 0){
                        System.out.println("Pilihan Movie :");
                        for(int i=0; i<movieList.size(); i++){
                            System.out.println((i+1) + ". " + movieList.get(i));
                        }
                        System.out.print("pilih >> ");
                        tonton = Integer.parseInt(input.nextLine());

                        if(tonton > 0 && tonton <= movieList.size()){
                            tonton -= 1;
                            Movie movie = movieList.get(tonton);

                            do{
                                System.out.print("Movie " + movie.getJudul() + " tersedia, apakah anda ingin bookmark atau menonton [1. Bookmark | 2. Menonton] : ");
                                confirm = input.nextLine().toLowerCase().charAt(0);
                            }while(confirm != '1' && confirm != '2');

                            if(confirm == '1'){
                                user.addBookmark(movie);
                                System.out.println("Movie " + movie.getJudul() + " berhasil di-bookmark");
                            }else{
                                if(movie.isTrailer()){
                                    do{
                                        System.out.print("Ingin menonton trailernya terlebih dahulu (y/n) : ");
                                        confirm = input.nextLine().toLowerCase().charAt(0);
                                    }while(confirm != 'y' && confirm != 'n');
                                        
                                    if(confirm == 'y'){
                                        movie.trailer();
        
                                        do{
                                            System.out.print("Apakah akan melanjutkan untuk menonton Movie " + movie.getJudul() + " (y/n) : ");
                                            confirm = input.nextLine().toLowerCase().charAt(0);
                                        }while(confirm != 'y' && confirm != 'n');
        
                                        if(confirm == 'y'){
                                            movie.play();
                                        }
                                    }else{
                                        movie.play();
                                    }
                                }else{
                                    movie.play();
                                }
                            }
                        }else{
                            System.out.println("Movie tidak ditemukan :(");
                        }
                    }else{
                        System.out.println("List movie belum tersedia :(");
                    }

                    break;
                }

                case 4:{
                    if(seriesList.size() > 0){
                        System.out.println("Pilihan Series :");
                        for(int i=0; i<seriesList.size(); i++){
                            System.out.println((i+1) + ". " + seriesList.get(i));
                        }
                        System.out.print("Pilih >> ");
                        tonton = Integer.parseInt(input.nextLine());

                        if(tonton > 0 && tonton <= seriesList.size()){
                            tonton -= 1;
                            Series series = seriesList.get(tonton);

                            do{
                                System.out.print("Series " + series.getJudulSeries() + " tersedia, apakah anda ingin bookmark atau menonton [1. Bookmark | 2. Menonton] : ");
                                confirm = input.nextLine().toLowerCase().charAt(0);
                            }while(confirm != '1' && confirm != '2');

                            if(confirm == '1'){
                                user.addBookmarkSeries(series);
                            }else{
                                if(series.isTrailer()){
                                    do{
                                        System.out.print("Ingin menonton trailernya terlebih dahulu (y/n) : ");
                                        confirm = input.nextLine().toLowerCase().charAt(0);
                                    }while(confirm != 'y' && confirm != 'n');

                                    if(confirm == 'y'){
                                        series.nontonTrailer();

                                        do{
                                            System.out.print("Apakah akan melanjutkan untuk menonton Series " + series.getJudulSeries() + " (y/n) : ");
                                            confirm = input.nextLine().toLowerCase().charAt(0);
                                        }while(confirm != 'y' && confirm != 'n');

                                        if(confirm == 'y'){
                                            System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                            for(int i=0; i<series.getSumEpisodes(); i++){
                                                System.out.println(series.getEpisode(i));
                                            }
                                            System.out.print(">> ");
                                            episode = Integer.parseInt(input.nextLine());

                                            System.out.println();
                                            if(episode > 0 && episode <= series.getSumEpisodes()){
                                                episode -= 1;
                                                series.nontonEpisodeSerie(episode);
                                            }else{
                                                System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                            }
                                        }
                                    }else{
                                        System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                        for(int i=0; i<series.getSumEpisodes(); i++){
                                            System.out.println(series.getEpisode(i));
                                        }
                                        System.out.print(">> ");
                                        episode = Integer.parseInt(input.nextLine());

                                        System.out.println();
                                        if(episode > 0 && episode <= series.getSumEpisodes()){
                                            episode -= 1;
                                            series.nontonEpisodeSerie(episode);
                                        }else{
                                            System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                        }
                                    }
                                }else{
                                    System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                    for(int i=0; i<series.getSumEpisodes(); i++){
                                        System.out.println(series.getEpisode(i));
                                    }
                                    System.out.print(">> ");
                                    episode = Integer.parseInt(input.nextLine());

                                    System.out.println();
                                    if(episode > 0 && episode <= series.getSumEpisodes()){
                                        episode -= 1;
                                        series.nontonEpisodeSerie(episode);
                                    }else{
                                        System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                    }
                                }
                            }
                        }else{
                            System.out.println("Series tidak ditemukan :(");
                        }
                    }else{
                        System.out.println("List series belum tersedia :(");
                    }
                    
                    break;
                }

                case 5:{
                    if(user.getBookmark().size() + user.getBookmarkSeriesList().size() > 0){
                        System.out.println("List bookmark :");
                        for(int i=0; i<user.getBookmark().size() + user.getBookmarkSeriesList().size(); i++){
                            if(i < user.getBookmark().size()){
                                System.out.println((i+1) + ". " + user.getBookmark().get(i));
                            }else{
                                System.out.println((i+1) + ". " + user.getBookmarkSeriesList().get(i - user.getBookmark().size()));
                            }
                        }
                        System.out.print("Pilih >> ");
                        tonton = Integer.parseInt(input.nextLine());

                        if(tonton > 0 && tonton <= user.getBookmark().size() + user.getBookmarkSeriesList().size()){
                            if(tonton <= user.getBookmark().size()){
                                tonton -= 1;
                                Movie movie = user.getBookmark().get(tonton);

                                do{
                                    System.out.print("Movie " + movie.getJudul() + " tersedia, apakah anda ingin hapus atau menonton [1. Hapus | 2. Menonton] : ");
                                    confirm = input.nextLine().toLowerCase().charAt(0);
                                }while(confirm != '1' && confirm != '2');

                                if(confirm == '1'){
                                    user.getBookmark().remove(tonton);
                                    System.out.println("Movie " + movie.getJudul() + " berhasil dihapus dari bookmark");
                                }else{
                                    if(movie.isTrailer()){
                                        do{
                                            System.out.print("Ingin menonton trailernya terlebih dahulu (y/n) : ");
                                            confirm = input.nextLine().toLowerCase().charAt(0);
                                        }while(confirm != 'y' && confirm != 'n');
                                            
                                        if(confirm == 'y'){
                                            movie.trailer();
            
                                            do{
                                                System.out.print("Apakah akan melanjutkan untuk menonton Movie " + movie.getJudul() + " (y/n) : ");
                                                confirm = input.nextLine().toLowerCase().charAt(0);
                                            }while(confirm != 'y' && confirm != 'n');
            
                                            if(confirm == 'y'){
                                                movie.play();
                                            }
                                        }else{
                                            movie.play();
                                        }
                                    }else{
                                        movie.play();
                                    }
                                }
                            }else{
                                tonton -= (1 + (user.getBookmark().size()));
                                Series series = user.getBookmarkSeriesList().get(tonton);
    
                                do{
                                    System.out.print("Series " + series.getJudulSeries() + " tersedia, apakah anda ingin hapus atau menonton [1. Hapus | 2. Menonton] : ");
                                    confirm = input.nextLine().toLowerCase().charAt(0);
                                }while(confirm != '1' && confirm != '2');
    
                                if(confirm == '1'){
                                    user.getBookmarkSeriesList().remove(tonton);
                                    System.out.println("Series " + series.getJudulSeries() + " berhasil dihapus dari bookmark");
                                }else{
                                    if(series.isTrailer()){
                                        do{
                                            System.out.print("Ingin menonton trailernya terlebih dahulu (y/n) : ");
                                            confirm = input.nextLine().toLowerCase().charAt(0);
                                        }while(confirm != 'y' && confirm != 'n');
    
                                        if(confirm == 'y'){
                                            series.nontonTrailer();
    
                                            do{
                                                System.out.print("Apakah akan melanjutkan untuk menonton Series " + series.getJudulSeries() + " (y/n) : ");
                                                confirm = input.nextLine().toLowerCase().charAt(0);
                                            }while(confirm != 'y' && confirm != 'n');
    
                                            if(confirm == 'y'){
                                                System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                                for(int i=0; i<series.getSumEpisodes(); i++){
                                                    System.out.println(series.getEpisode(i));
                                                }
                                                System.out.print(">> ");
                                                episode = Integer.parseInt(input.nextLine());
    
                                                System.out.println();
                                                if(episode > 0 && episode <= series.getSumEpisodes()){
                                                    episode -= 1;
                                                    series.nontonEpisodeSerie(episode);
                                                }else{
                                                    System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                                }
                                            }
                                        }else{
                                            System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                            for(int i=0; i<series.getSumEpisodes(); i++){
                                                System.out.println(series.getEpisode(i));
                                            }
                                            System.out.print(">> ");
                                            episode = Integer.parseInt(input.nextLine());
    
                                            System.out.println();
                                            if(episode > 0 && episode <= series.getSumEpisodes()){
                                                episode -= 1;
                                                series.nontonEpisodeSerie(episode);
                                            }else{
                                                System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                            }
                                        }
                                    }else{
                                        System.out.println("List Episode Series " + series.getJudulSeries() + " :");
                                        for(int i=0; i<series.getSumEpisodes(); i++){
                                            System.out.println(series.getEpisode(i));
                                        }
                                        System.out.print(">> ");
                                        episode = Integer.parseInt(input.nextLine());
    
                                        System.out.println();
                                        if(episode > 0 && episode <= series.getSumEpisodes()){
                                            episode -= 1;
                                            series.nontonEpisodeSerie(episode);
                                        }else{
                                            System.out.println(series.getJudulSeries() + " Episode " + episode + " Tidak ditemukan :(");
                                        }
                                    }
                                }
                            }
                        }else{
                            System.out.println("Bookmark tidak ditemukan");
                        }
                    }else{
                        System.out.println("Bookmark masih kosong!");
                    }

                    break;
                }

                case 6:{
                    System.out.println("Anda telah Log Out...");

                    break;
                }

                default:{
                    System.out.println("Pilihan Tidak Valid!");

                    break;
                }
            }

            if(choice != 6){
                System.out.println();
            }
        }while(choice != 6);
    }

    public void run(){
        int choice;
        String email, password;

        do{
            System.out.println("=== SELAMAT DATANG DI VIU ===");
            System.out.println("1. Login");
            System.out.println("2. Sign Up");
            System.out.println("3. Keluar");
            System.out.print(">> ");

            choice = Integer.parseInt(input.nextLine());

            System.out.println();
            switch(choice){
                case 1:{
                    System.out.print("Masukkan Email    : ");
                    email = input.nextLine();

                    System.out.print("Masukkan Password : ");
                    password = input.nextLine();

                    if(login(email, password)){
                        main(getUser(email, password));
                    }else{
                        System.out.println("Gagal Login! Email atau Password Salah");
                    }

                    break;
                }

                case 2:{
                    System.out.print("Masukkan Email    : ");
                    email = input.nextLine();

                    System.out.print("Masukkan Password : ");
                    password = input.nextLine();

                    if(signup(email, password)){
                        System.out.println("Berhasil Sign Up!");
                    }else{
                        System.out.println("Gagal Sign Up! Email Telah Terdaftar");
                    }

                    break;
                }

                case 3:{
                    System.out.println("Keluar dari Viu...");

                    break;
                }

                default:{
                    System.out.println("Pilihan Tidak Valid!");
                    
                    break;
                }
            }

            if(choice != 3){
                System.out.println();
            }
        }while(choice != 3);

        input.close();
    }
}