public class Movie extends Video {
    private boolean trailer;

    public Movie(String judul, boolean trailer) {
        super(judul);
        this.trailer = trailer;
    }

    public boolean isTrailer() {
        return trailer;
    }

    public void setTrailer(boolean trailer) {
        this.trailer = trailer;
    }

    public void trailer() {
        if(trailer){
            System.out.println("Sedang memutar trailer movie " + judul);
        }else{
            System.out.println("Movie " + judul + " tidak memiliki trailer");
        }
    }

    @Override
    public void play() {
        System.out.println("Sedang memutar movie " + judul);
    }

    @Override
    public String toString() {
        return getJudul();
    }
}
