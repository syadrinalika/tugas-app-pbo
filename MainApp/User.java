import java.util.ArrayList;

public class User {
    private String email;
    private String password;
    private ArrayList<Movie> bookmarkList;
    private ArrayList<Series> bookmarkSeriesList;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
        bookmarkList = new ArrayList<>();
        bookmarkSeriesList = new ArrayList<>();
    }

    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Movie> getBookmark() {
        return bookmarkList;
    }

    public void addBookmark(Movie movie) {
        this.bookmarkList.add(movie);
    }

    public ArrayList<Series> getBookmarkSeriesList() {
        return bookmarkSeriesList;
    }

    public void addBookmarkSeries(Series series) {
        this.bookmarkSeriesList.add(series);
    }
}
