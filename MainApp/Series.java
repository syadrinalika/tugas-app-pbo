import java.util.ArrayList;

public class Series {
    private String judulSeries;
    private ArrayList<EpisodeSeries> episodeSeriesList;
    private boolean trailer;

    public Series(String judulSeries, String judul[], boolean trailer){
        this.judulSeries = judulSeries;
        episodeSeriesList = new ArrayList<>();
        this.trailer = trailer;

        for(int i=0; i<judul.length; i++){
            episodeSeriesList.add(new EpisodeSeries(judul[i], i+1));
        }
    }

    public boolean isTrailer() {
        return trailer;
    }

    public void setTrailer(boolean trailer) {
        this.trailer = trailer;
    }

    public void setJudulSeries(String judulSeries){
        this.judulSeries = judulSeries;
    }

    public String getJudulSeries(){
        return judulSeries;
    }

    public void nontonEpisodeSerie(int episode){
        System.out.print("Sedang memutar " + judulSeries);
        episodeSeriesList.get(episode).play();
    }

    public EpisodeSeries getEpisode(int episode){
        return episodeSeriesList.get(episode);
    }

    public int getSumEpisodes(){
        return episodeSeriesList.size();
    }

    public void nontonTrailer() {
        if(trailer){
            System.out.println("Sedang memutar trailer series " + getJudulSeries());
        }else{
            System.out.println("Series " + getJudulSeries() + " tidak memiliki trailer");
        }
    }

    @Override
    public String toString(){
        return judulSeries + " (" + episodeSeriesList.size() + " Episode)";
    }
}
